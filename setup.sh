#This script does the setup

##MAKE SURE YOU CHANGE THE LINES WITH "CHANGEME"

#Check if this script is running as root
if [ "$(id -u)" != "0" ]; then
echo "This script must be run as root" 1>&2
exit 1
fi

echo "Updating & Upgrading standard installation"
yum -y update
yum -y upgrade

echo "Installing git stuff if not done already"
yum install -y git git-gui gitk curl wget

echo "Install RPM Fusion repo" #Needed for VLC
yum localinstall --nogpgcheck http://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm http://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm

echo "Downloading files for VirtualBox"
rm Oracle_VM_VirtualBox_Extension_Pack-4.2.10-84104.vbox-extpack
wget http://download.virtualbox.org/virtualbox/4.2.10/Oracle_VM_VirtualBox_Extension_Pack-4.2.10-84104.vbox-extpack
wget http://download.virtualbox.org/virtualbox/rpm/fedora/virtualbox.repo
mv virtualbox.repo /etc/yum.repos.d/virtualbox.repo

echo "Install other utils"
yum install bleachbit vlc VirtualBox-4.2 blender thunderbird firefox azureus rsync rsnapshot k3b unetbootin boinc-manager boinc-client https://dl.google.com/linux/direct/google-chrome-stable_current_i386.rpm https://dl.google.com/linux/direct/google-musicmanager-beta_current_i386.rpm ssh sshfs ffmpeg mencoder gcc

#CHANGEME: Users must be members of vboxusers to be able to use VMs properly.
#If the user is you, or is already logged in, they will need to log out and back in again.
useradd -G vboxusers {usernamehere}

echo "Installing Extension For VirtualBox"
sudo virtualbox Oracle_VM_VirtualBox_Extension_Pack-4.2.10-84104.vbox-extpack
rm Oracle_VM_VirtualBox_Extension_Pack-4.2.10-84104.vbox-extpack

#CHANGEME: change this section to include the user(s) you want to be able to use the BOINC Manager.
#If the user is you, or is already logged in, they will need to log out and back in again.

echo "Configuring BOINC"

/usr/sbin/usermod -G boinc -a {usernamehere}
chmod g+rw /var/lib/boinc
chmod g+rw /var/lib/boinc/*.*
ln -s /var/lib/boinc/gui_rpc_auth.cfg /home/{usernamehere}/gui_rpc_auth.jpg

echo "Done."