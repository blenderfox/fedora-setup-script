#This script is to add all keys found in the current user's .ssh folder (you can change that folder if needed).
#This is used in case you add some git pulls to the setup script and ssh-add doesn't keep the previously added keys.

echo "Adding keys found in ~/.ssh/"
for a in `find ~/.ssh/ | egrep -v '\.pub$' | egrep -v 'known_hosts' | egrep -v 'authorized_keys'`
do
  if [ -f $a ]; then
    chmod 600 $a
    ssh-add $a
  fi
done

echo ""
echo "Current keys loaded:"
ssh-add -l